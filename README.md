## mosaik Java Api

This is an implementation of the mosaik API for simulators written in Java.
It hides all the messaging and networking related stuff and provides a 
simple base class that you can implement.
For more information about mosaik, visit [mosaiks-documentation].

[mosaiks-documentation]: https://mosaik.readthedocs.io/en/latest/
## Installation 

### Usage
To use this package in your project, 
you can add it via Gradle by including it to your dependencies and 
add the gitlab package repository to your maven repositories  
```groovy
dependency {
    implementation 'de.offis.mosaik.api:mosaik-api-java:<CURR_VERSION>'
}

maven {
    url 'https://gitlab.com/api/v4/projects/16916947/packages/maven'
}
```

### Installation for development
If you want to clone this project to contribute to the project, you have two prerequisites:
  - python3 installed
  - pip in your path environment variable
  
The build job is done with 
```python
./gradlew shadowJar OR gradle jar
```

## Testing 
To run the test cases, you will need Python 3 and
the virtualenv package for your current Python environment installed.
You can then, if you are working on Linux or OS X, run the tests via
```python
$ ./runtests.sh [-s]
```
or if you working on a windows machine:
```python
$  runtests.bat "path-to-Python-3-executable" [-s]
```
This will build the API and the test simulators using gradle, create a new
Python 3 virtualenv with all required packages installed, let Pytest run the test cases, and finish with some cleanup. You can optionally pass the
parameter -s to activate Pytests console output for additional debug
information.

## Documentation 

You can find general information about the API in the [API-documentation]. Also, all public classes and methods also have docstrings (there is no pre-built Java doc
yet).

The `tests/` directory contains an example simulator (ExampleSim) and an
example control strategy (ExampleMAS) that may give you an idea of what to do
and how things work.

[API-documentation]: https://mosaik.readthedocs.org/en/latest/mosaik-api/

## Support

If you have a question, a feature request, or spotted a bug please write an issue [here].

[here]: https://gitlab.com/mosaik/api/mosaik-api-java/-/issues
